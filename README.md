# Useful links from Kafka documentation

- Broker configs:
https://kafka.apache.org/documentation/#brokerconfigs

- Topic-Level configs:
https://kafka.apache.org/documentation/#topicconfigs

- Producer Configs:
https://kafka.apache.org/documentation/#producerconfigs

- Consumer Configs:
https://kafka.apache.org/documentation/#consumerconfigs


# Useful links from Confluent-Kafka library documentation

- https://docs.confluent.io/current/clients/confluent-kafka-python/index.html

- Configuration fields for both Consumer and Producer (confluent-kafka library):
https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md