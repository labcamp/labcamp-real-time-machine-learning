### ACTIVATE KAFKA DOCKER ###
docker run -p 2181:2181 -p 3030:3030 -p 8081:8081 -p 8082:8082 -p 8083:8083 -p 9092:9092 -e ADV_HOST=127.0.0.1 -e BROWSECONFIGS=1 -v /var/tmp/kafka_data:/data landoop/fast-data-dev:1.0.1

### STEP ONBOARD OF THE DOCKER ###
docker run --rm -it landoop/fast-data-dev:1.0.1 bash

### CREATE KAFKA TOPICS ###
/usr/local/bin/kafka-topics --create --zookeeper 127.0.0.1:2181 --replication-factor 1 --partitions 1 --topic stream-dataset

/usr/local/bin/kafka-topics --create --zookeeper 127.0.0.1:2181 --replication-factor 1 --partitions 1 --topic stream-cleaned-data

/usr/local/bin/kafka-topics --create --zookeeper 127.0.0.1:2181 --replication-factor 1 --partitions 1 --topic stream-discarded-data


### CLONE THE LABCAMP REPO ###
mkdir ~/Development/labcamp-real-time-ml
cd ~/Development
git clone https://gitlab.com/labcamp/labcamp-real-time-machine-learning.git

### UPDATE THE LABCAMP REPO ###
cd ~/labcamp-real-time-ml
git pull

### ACTIVATE PY VIRTUALENV ###
source ~/Development/labcamp-real-time-ml/env/bin/activate

### LAUNCH JUPYTER LAB ###
jupyter-lab

